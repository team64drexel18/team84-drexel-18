<?php
session_start();
include 'dbh.php';

if (isset($_POST['submit'])) {
	include 'dbh.php';
	
	$username = mysqli_real_escape_string($conn, $_POST['username']);
	$password = mysqli_real_escape_string($conn,$_POST['password']);
	
	//Error Handling
	// If fields are empty
	if(empty($username) || empty($password)) {
		header("Location: CryptoLogin.html?login=empty");
		exit();
	} else {
		// If username exists
		$sql = "SELECT * FROM users WHERE username='$username'";
		$result = mysqli_query($conn, $sql);
		$resultcheck = mysqli_num_rows($result);
		if ($resultcheck < 1) {
			header("Location: CryptoLogin.html?login = error");
			exit();
		} else {
			if($row = mysqli_fetch_assoc($result)) {
				//De-hash the password in the database to check against the password the user entered.
				$hashedpasswordcheck = password_verify($password, $row['password']);
			if ($hashedpasswordcheck == false) {
				header("Location: CryptoLogin.html?login = error");
				exit();			
				}
			} elseif ($hashedpasswordcheck == true) {
				//Checks are completed and password
				$_SESSION['id'] = $row['id'];
				$_SESSION['email'] = $row['email'];
				$_SESSION['username'] = $row['username'];
				header("Location: CryptoMyCoinsLoggedIn.html?login = success");
				exit();
			}
		}
	}
} else {
	header("Location: CryptoLogin.html?login=invalid");
	exit();
}
header("Location: CryptoMyCoinsLoggedIn.html?login = success");
exit();
?>