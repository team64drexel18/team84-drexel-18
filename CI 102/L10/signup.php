<?php
session_start();

if(isset($_POST['submit'])){
	
	include 'dbh.php';
		
	$email = mysqli_real_escape_string($conn, $_POST['email']);
	$username = mysqli_real_escape_string($conn,$_POST['username']);
	$password = mysqli_real_escape_string($conn,$_POST['password']);
	
	//Error Handlers
	//Check for empty fields ***********
	if (empty($email) || empty($username) || empty($password)){
			header("Location: CryptoRegister.html?signup=empty");
			exit();
	} else {
		//Check if characters are valid
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			header("Location: CryptoRegister.html?signup=invalidemail");
			exit();
		} else {
			$sql = "SELECT * FROM users WHERE username = '$username'";
			$result = mysqli_query($conn, $sql);
			$resultcheck = mysqli_num_rows($result);
			
			if($resultcheck > 0) {
				header("Location: CryptoRegister.html?signup=userexists");
				exit();
			}	else {
				//Hashing password
				$hashedpswd = password_hash($password, PASSWORD_DEFAULT);
				// Insert the the user into database
				$sql = "INSERT INTO users (email, username, password) VALUES ('$email', '$username', '$hashedpswd')";
				mysqli_query($conn, $sql);
				header("Location: CryptoLogin.html?signup=success");
				exit();
			}
		}	
	}	
		
} else {
	header("Location: CryptoRegister.html");
	exit();
}